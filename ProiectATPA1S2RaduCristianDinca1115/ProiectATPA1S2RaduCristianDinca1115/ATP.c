﻿#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
/*
Proiect pt. cursul de Algoritmi si Tehnici de Programare din cadrul ASE, 
facultatea de Cibernetica, Statistica si Informatica Economica, Anul 1.

Radu-Cristian Dinca, seria 1115/B

Aplicatia curenta are scopul de a oferi utilizatorului posibilitatea de 
a crea fisiere binare si de a adauga si sterge articole.

Articolele sunt structuri din limbajul C cu doua campuri:

1.	Un camp de tip int.
2.	Un camp de tip string.

Aplicatia este structurata in cateva "stari" predefinte, fiecare stare
oferind utilizatorului diferite optiuni. Optiunile sunt accesate prin 
tiparirea anumitor comenzi specifice:

1.	Meniu Principal:
		fnou	--- creaza un fisier nou.
		Trebuie introdus numele fisierului care va fi creat.

		fmod	--- modifica un fisier existent.
		Trebuie introdus numele unui fisier existent pentru a
		fi editat.

		frpt	--- raportare despre un fisier existent.
		Permite utilizatorului posibilitatea de a genera un raport
		in fisier text care va contine toate articolele din fisier si va
		afisa toate campurile din articol.

		exit	--- inchide programul sau bucla curenta.
2.	Meniu Modificare Fisier:
		adg	--- creaza un articol nou.
		Permite utilizatorului sa introduca un articol nou in fisier, 
		impreuna cu valorile campurilor din articole.
		stg	--- sterge un articol existent.
		Sterge un articol dupa numarul de ordine din fisier.
		mpr	--- inchide bucla curenta.
		Imapoi la Meniul principal.



*/
#pragma region Definitii

char const CMD_IESIRE[] = { "exit\n" };
char const CMD_FISIER_NOU[] = { "fnou\n" };
char const  CMD_MODIFICARE_FISIER[] = { "fmod\n" };
char const  CMD_RAPORT_COMPLET[] = { "frpt\n" };
char const  CMD_ADAUGA_ARTICOL[] = { "fadg\n" };
char const  CMD_STERGE_ARTICOL[] = { "fstg\n" };
char const  CMD_MENIU_PRINCIPAL[] = { "fmpr\n" };

char const  TITLU_TEMA[] = { "\n\t ||====================================================================||\n\t||\t\t\t\tProiect ATP\t\t\t\t||\n\t||\taplicatie simpla de creare si editare de fisiere\t\t||\n\t||\t\t\t\t\t\t\t\t\t||\n\t||\t\t\tRadu-Cristian Dinca, Anul 1, Grupa 1115\t\t||\n\t ||====================================================================||\n\n" };

char const  MENIU_PRINCIPAL[] = { "Meniu principal:\n\nTasteaza o comanda:\nfnou	--- creaza un fisier nou.\nfmod	--- modifica un fisier existent.\nfrpt	--- raportare despre un fisier existent.\nexit	--- inchide programul sau bucla curenta.\n" };
char const  MENIU_EDITARE_FISIER[] = { "Meniu editare fisier:\n\nTasteaza o comanda:\nfadg	--- creaza un articol nou.\nfmdf	--- modifica un articol existent.\nfstg	--- sterge un articol existent.\nfmpr	--- meniu principal.\n" };
char const  MENIU_RAPORTARE[] = { "Raportare fisier:\n\nTasteaza o comanda:\nfrpt	--- generare raport.\nfmpr	--- inchide bucla curenta.\n" };

char const  INFO_EROARE_FISIER[] = { "Fisierul nu exista sau nu a putut fi deschis!\n" };
char const  INFO_EROARE_INPUT_INT[] = { "EROARE! Nu ati introdus un numar intreg!" };
char const  IFNO_NR_CRT_ARTICOLE[] = { "Nr. curent articole este: %d\n" };
char const  INFO_AFISEAZA_NOTA[] = { "nota veche %i:\t\n" };
char const  INFO_AFISEAZA_PRET[] = { "pret vechi %i:\t\n" };
char const  INFO_AFISEAZA_NUME[] = { "titlu vechi %s:\t\n" };
char const  INFO_AFISEAZA_VERSIUNE[] = { "versiune veche:\t%i%c%i%c%i%c%i" };
char const  INFO_AFISEAZA_NR_ART[] = { "Fisierul contine %i articole.\n" };
char const  INFO_MENIU_MDF[] = { "Tastati numarul articolului pe care doriti sa il modificati.\nApasati tasta Enter pentru a continua sa adaugati articole pe care doriti sa le modificati. Tastati fmpr pentru a iesi din bucla.\nMomentat ati adaugat %i articole.\n" };
char const  INFO_INTRODUCETI_NUME_FISIER[] = { "Nume Fisier: (Max 16 char)\n" };
char const  INFO_INTRODUCETI_NUME_RAPORT[] = { "Nume fisier de raport: (Max 16 char)\n" };
char const  IFNO_INTRODUCETI_INDEX_STERGERE[] = { "Introduceti numarul articolului pe care doriti sa il stergeti: (se recomanda un nr. natural mai mic sau egal cu nr. maxim de fisiere.)\n" };
char const  INFO_INTRODUCETI_VALORI_INDEX_FISIER[] = { "Introduceti valori pt. articolul nr. %d din fisier:\n" };
char const  INFO_INTRODUCETI_VALORI_CAMP_NOTA[] = { "Introduceti nota primita de produs de la critici (valoarea se recomanda a fi de tipul int):\n" };
char const  INFO_INTRODUCETI_VALORI_CAMP_PRET[] = { "Introduceti pretul produsului (valoarea se recomanda a fi de tipul int):\n" };
char const  INFO_INTRODUCETI_VALORI_CAMP_TITLU[] = { "Introduceti titlul produsului (valoarea se recomanda a fi de tipul char[], cu lungimea maxima 16):" };
char const  INFO_INTRODUCETI_VALORI_CAMP_VERS[] = { "Introduceti versiunea produsului de formatul 1234 folosind MAXIMUM 4 caractere (e.g. v4.0.1.2 se va scrie 4012):\n" };

int const LUNGIME_COMANDA_MAXIMA = 16;

/*
Tip de date custom creat pt. tema ATP.
*/
typedef struct  
{
	int notaReview;
	int pret;
	char numeProdus[16];
	int versiuneProdus[4];
}DetaliiProdus;

char INPUT_UTILIZATOR[16] = "";

FILE * FISIER = NULL;

#pragma endregion
/*
Metoda de adaugare articol.
*/
void adaugaArticol(char numeFisier[])
{
	DetaliiProdus articol;
	int nrArticol = numarArticole(numeFisier);
	if (nrArticol < 0)
	{
		printf(INFO_EROARE_FISIER);
		return;
	}
	FISIER = fopen(numeFisier, "a+b");
	printf(INFO_INTRODUCETI_VALORI_INDEX_FISIER, nrArticol);
	//nota
	printf(INFO_INTRODUCETI_VALORI_CAMP_NOTA);	
	fgets(INPUT_UTILIZATOR,LUNGIME_COMANDA_MAXIMA, stdin);
	if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
		return NULL;	
	articol.notaReview = atoi(INPUT_UTILIZATOR);
	//pret
	printf(INFO_INTRODUCETI_VALORI_CAMP_PRET);
	fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
	if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
		return NULL;
	articol.pret = atoi(INPUT_UTILIZATOR);
	//titlu
	printf(INFO_INTRODUCETI_VALORI_CAMP_TITLU);
	fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
	if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
		return NULL;
	strncpy(articol.numeProdus, INPUT_UTILIZATOR, sizeof(articol.numeProdus));
	//versiune
	printf(INFO_INTRODUCETI_VALORI_CAMP_VERS);
	fgets(INPUT_UTILIZATOR, 5, stdin);
	if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
		return NULL;
	for (int i = 0; i < 4; i++)
	{
		int v = INPUT_UTILIZATOR[i] - 0;
		articol.versiuneProdus[i] = atoi(&v);
	}

	fwrite(&articol, sizeof(DetaliiProdus), 1, FISIER);
	fclose(FISIER);
}
/*
metoda care raporteaza intern (dpdv al softului rulat) numarul de articole din fisier.
*/
int numarArticole(char numeFisier[])
{
	FISIER = fopen(numeFisier, "r");
	int eroare = verificaFisier();
	if (eroare != 0)
		return eroare;
	fseek(FISIER, 0, SEEK_END);
	long marimeFisier = ftell(FISIER);
	long marimeArticol = sizeof(DetaliiProdus);
	fclose(FISIER);
	return marimeFisier / marimeArticol;
}

int verificaFisier()
{
	if (FISIER == NULL)
		return -1;

	return 0;
}


void modificaArticol(int nrInFisier[], int nrArticole, char numeFisier[])
{
	DetaliiProdus articole[4096];
	FISIER = fopen(numeFisier, "r");
	for (int i = 0; i < nrArticole; i++)
		fread(&articole[i], 1, sizeof(DetaliiProdus), FISIER);
	fclose(FISIER);
	for (int i = 0; i < nrArticole; i++)
		if (nrInFisier[i] > 0 && nrInFisier[i] < nrArticole)
		{
			printf(INFO_INTRODUCETI_VALORI_INDEX_FISIER, nrInFisier[i]);
			//nota
			printf(INFO_AFISEAZA_NOTA, articole[i].notaReview);
			printf(INFO_INTRODUCETI_VALORI_CAMP_NOTA);
			fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
			if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
				return NULL;
			articole[i].notaReview = atoi(INPUT_UTILIZATOR);
			//pret
			printf(INFO_AFISEAZA_PRET, articole[i].pret);
			printf(INFO_INTRODUCETI_VALORI_CAMP_PRET);
			fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
			if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
				return NULL;
			articole[i].pret = atoi(INPUT_UTILIZATOR);
			//titlu
			printf(INFO_AFISEAZA_NUME, articole[i].numeProdus);
			printf(INFO_INTRODUCETI_VALORI_CAMP_TITLU);
			fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
			if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
				return NULL;
			strncpy(articole[i].numeProdus, INPUT_UTILIZATOR, sizeof(articole[i].numeProdus));
			//versiune
			printf(INFO_AFISEAZA_VERSIUNE,
				articole[i].versiuneProdus[0], '.', articole[i].versiuneProdus[1], '.', articole[i].versiuneProdus[2], '.', articole[i].versiuneProdus[3]);
			printf(INFO_INTRODUCETI_VALORI_CAMP_VERS);
			fgets(INPUT_UTILIZATOR, 5, stdin);
			if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
				return NULL;
			for (int i = 0; i < 4; i++)
			{
				int v = INPUT_UTILIZATOR[i] - 0;
				articole[i].versiuneProdus[i] = atoi(&v);
			}
		}
	FISIER = fopen(numeFisier, "w+b");
	fwrite(articole, sizeof(DetaliiProdus), nrArticole, FISIER);
	fclose(FISIER);
}

void proceseazaInputNrArticole(char numeFisier[])
{
	int nrArticole = numarArticole(numeFisier);
	int deSters[4096];
	printf(INFO_AFISEAZA_NR_ART, nrArticole);
	int i = 0;
	do
	{
		printf(INFO_MENIU_MDF, i);
		fgets(INPUT_UTILIZATOR, 5, stdin);
		if (feof(stdin))
		{
			fflush(stdin);
			break;
		}

		deSters[i] = atoi(INPUT_UTILIZATOR);
		printf("Articolul nr. %i va fi moficiat!\n", deSters[i]);
		i++;
	} while (i < nrArticole && strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL));
	modificaArticol(deSters, nrArticole, numeFisier);
}



/*
Metoda care sterge un articol din fisier.
*/
void stergeArticol(char numeFisier[])
{
	int nrArticole = numarArticole(numeFisier);
	printf(IFNO_NR_CRT_ARTICOLE, nrArticole);
	printf(IFNO_INTRODUCETI_INDEX_STERGERE);
	fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
	int nrArticolDeSters = atoi(INPUT_UTILIZATOR);
	if (!nrArticolDeSters)
	{
		printf(INFO_EROARE_INPUT_INT);
		return NULL;
	}
	FISIER = fopen(numeFisier, "r");
	DetaliiProdus articole[4096];
	for (int i = 0; i < nrArticole; i++)
		if (i != nrArticolDeSters)
			fread(&articole[i], 1, sizeof(DetaliiProdus), FISIER);
	fclose(FISIER);
	FISIER = fopen(numeFisier, "w+b");
	fwrite(articole, sizeof(DetaliiProdus), nrArticole - 1, FISIER);
	fclose(FISIER);
}
/*
Metoda care genereaza un fisier text care contine toate articolele din fisierul dat.
*/
void raportText()
{
	printf(MENIU_RAPORTARE);
	fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
	if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
		return NULL;
	else if (strcmp(INPUT_UTILIZATOR, CMD_RAPORT_COMPLET) == 0)
	{
		printf(INFO_INTRODUCETI_NUME_FISIER);
		fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
		if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
			return NULL;
		INPUT_UTILIZATOR[strlen(INPUT_UTILIZATOR) - 1] = '\0';
		int nrArticole = numarArticole(INPUT_UTILIZATOR);
		if (nrArticole < 0)
		{
			printf(INFO_EROARE_FISIER);
			return;
		}
		FISIER = fopen(INPUT_UTILIZATOR, "r");
		DetaliiProdus articole[4096];
		for (int i = 0; i < nrArticole; i++)
			fread(&articole[i], 1, sizeof(DetaliiProdus), FISIER);
		fclose(FISIER);
		printf(INFO_INTRODUCETI_NUME_RAPORT);
		fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
		INPUT_UTILIZATOR[strlen(INPUT_UTILIZATOR) - 1] = '\0';
		strcat(INPUT_UTILIZATOR, ".txt");
		FISIER = fopen(INPUT_UTILIZATOR, "w");
		for (int i = 0; i < nrArticole; i++)
		{

			fprintf(FISIER, "nota review:\t%i", articole[i].notaReview);
			fputs("\n", FISIER);

			fprintf(FISIER, "pret produs:\t%i", articole[i].pret);
			fputs("\n", FISIER);

			fprintf(FISIER, "nume produs:\t%s", articole[i].numeProdus);
			fputs("\n", FISIER);

			fprintf(FISIER, "versiune produs:\t%i%c%i%c%i%c%i", 
				articole[i].versiuneProdus[0], '.', articole[i].versiuneProdus[1], '.', articole[i].versiuneProdus[2], '.', articole[i].versiuneProdus[3]);
			fputs("\n", FISIER);


			fputs("====================\n", FISIER);
		}
		fclose(FISIER);
	}
	else if (strcmp(INPUT_UTILIZATOR, "frptd\n") == 0)
	{
		printf(INFO_INTRODUCETI_NUME_FISIER);
		fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
		if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
			return NULL;
		INPUT_UTILIZATOR[strlen(INPUT_UTILIZATOR) - 1] = '\0';
		int nrArticole = numarArticole(INPUT_UTILIZATOR);
		if (nrArticole < 0)
		{
			printf(INFO_EROARE_FISIER);
			return;
		}
		FISIER = fopen(INPUT_UTILIZATOR, "r");
		int deRaportat[4096];
		printf("Fisierul contine %i articole.\n", nrArticole);
		int i = 0;
		do
		{
			printf("Tastati numarul articolului pe care doriti sa il vedeti raportat.\nApasati tasta Enter pentru a continua sa adaugati articole pe care doriti sa le listati. Apasato Ctrl-Z, apoi Enter pentru a iesi din bucla.\nMomentat ati adaugat %i articole.\n", i);
			fgets(INPUT_UTILIZATOR, 5, stdin);

			deRaportat[i] = atoi(INPUT_UTILIZATOR);
			printf("Articolul nr. %i va fi sters!\n", deRaportat[i]);
			i++;
		} while (i < nrArticole && strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) != 0);
		DetaliiProdus articole[4096];
		for (int i = 0; i < nrArticole; i++)
			if (i == deRaportat[i])
				fread(&articole[i], 1, sizeof(DetaliiProdus), FISIER);
		fclose(FISIER);
		printf(INFO_INTRODUCETI_NUME_RAPORT);
		fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
		INPUT_UTILIZATOR[strlen(INPUT_UTILIZATOR) - 1] = '\0';
		strcat(INPUT_UTILIZATOR, ".txt");
		FISIER = fopen(INPUT_UTILIZATOR, "w");
		for (int i = 0; i < nrArticole; i++)
			if (i == deRaportat[i])
			{

				fprintf(FISIER, "nota review:\t%i", articole[i].notaReview);
				fputs("\n", FISIER);

				fprintf(FISIER, "pret produs:\t%i", articole[i].notaReview);
				fputs("\n", FISIER);

				fprintf(FISIER, "nume produs:\t%s", articole[i].numeProdus);
				fputs("\n", FISIER);

				fprintf(FISIER, "versiune produs:\t%i%c%i%c%i%c%i",
					articole[i].versiuneProdus[0], '.', articole[i].versiuneProdus[1], '.', articole[i].versiuneProdus[2], '.', articole[i].versiuneProdus[3]);
				fputs("\n", FISIER);


				fputs("====================\n", FISIER);
			}
		fclose(FISIER);
	}
	else if (strcmp(INPUT_UTILIZATOR, "frpte\n") == 0)
	{
		printf(INFO_INTRODUCETI_NUME_FISIER);
		fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
		if (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) == 0)
			return NULL;
		INPUT_UTILIZATOR[strlen(INPUT_UTILIZATOR) - 1] = '\0';
		int nrArticole = numarArticole(INPUT_UTILIZATOR);
		if (nrArticole < 0)
		{
			printf(INFO_EROARE_FISIER);
			return;
		}
		FISIER = fopen(INPUT_UTILIZATOR, "r");
		DetaliiProdus articole[4096];
		for (int i = 0; i < nrArticole; i++)
			fread(&articole[i], 1, sizeof(DetaliiProdus), FISIER);
		fclose(FISIER);
		char titluCautat[16];
		printf("Intruduceti titlul cautat:\n");
		fgets(titluCautat, LUNGIME_COMANDA_MAXIMA, stdin);
		for (int i = 0; i < nrArticole; i++)
		{
			if (strcmp(articole[i].numeProdus, titluCautat) == 0)
			{
				printf("nota review:\t%i", articole[i].notaReview);
				printf("pret produs:\t%i", articole[i].notaReview);
				printf("nume produs:\t%s", articole[i].numeProdus);
				printf("versiune produs:\t%i%c%i%c%i%c%i",
					articole[i].versiuneProdus[0], '.', articole[i].versiuneProdus[1], '.', articole[i].versiuneProdus[2], '.', articole[i].versiuneProdus[3]);
			}
		}
	}

}
/*
Metoda care creaza un fisier nou fara continut.
*/
void creazaFisier()
{
	printf(INFO_INTRODUCETI_NUME_FISIER);
	fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
	if (strcmp(INPUT_UTILIZATOR, CMD_IESIRE) == 0)
		return NULL;
	INPUT_UTILIZATOR[strlen(INPUT_UTILIZATOR) - 1] = '\0';
	FISIER = fopen(INPUT_UTILIZATOR, "w+b");
	if (FISIER != NULL)
		fclose(FISIER);
}

/*
Metoda care permite utilizatorului sa modifice un fisier existent.
*/
void modificaFisier()
{
	char numeFisier[16];
	printf(INFO_INTRODUCETI_NUME_FISIER);
	fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);
	if (strcmp(INPUT_UTILIZATOR, CMD_IESIRE) == 0)
		return NULL;
	INPUT_UTILIZATOR[strlen(INPUT_UTILIZATOR) - 1] = '\0';
	strncpy(numeFisier, INPUT_UTILIZATOR, sizeof(INPUT_UTILIZATOR));
	int nrArticol = numarArticole( numeFisier);
	if (nrArticol < 0)
	{
		printf(INFO_EROARE_FISIER);
		return;
	}
	do 
	{
		printf(MENIU_EDITARE_FISIER);

		fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);

		if (strcmp(INPUT_UTILIZATOR, CMD_ADAUGA_ARTICOL) == 0)
			adaugaArticol(numeFisier, &nrArticol);
		else if (strcmp(INPUT_UTILIZATOR, "fmdf\n") == 0)
			proceseazaInputNrArticole(numeFisier, &nrArticol);
		else if (strcmp(INPUT_UTILIZATOR, CMD_STERGE_ARTICOL) == 0)
			stergeArticol(INPUT_UTILIZATOR, FISIER, numeFisier);

	} while (strcmp(INPUT_UTILIZATOR, CMD_MENIU_PRINCIPAL) != 0);
}
/*
Afiseaza meniul principal si proceseaza operatiile.
*/
void meniuPrincipal() 
{
	do 
	{
		printf(MENIU_PRINCIPAL);

		fgets(INPUT_UTILIZATOR, LUNGIME_COMANDA_MAXIMA, stdin);

		if (strcmp(INPUT_UTILIZATOR, CMD_FISIER_NOU) == 0)
			creazaFisier(INPUT_UTILIZATOR);
		else if (strcmp(INPUT_UTILIZATOR, CMD_MODIFICARE_FISIER) == 0)
			modificaFisier(INPUT_UTILIZATOR);
		else if (strcmp(INPUT_UTILIZATOR, CMD_RAPORT_COMPLET) == 0)
			raportText(INPUT_UTILIZATOR);
	} 
	while (strcmp(INPUT_UTILIZATOR, CMD_IESIRE) != 0);
}
/*
Punct intrare aplicatie.
*/
void main()
{
	printf(TITLU_TEMA);
	meniuPrincipal();
}